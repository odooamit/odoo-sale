# README #

This Repository contains our Odoo Apps made for optimizations of sale processes.

# Content #

- **sale_order_line_fixed_sequence** A small helper module to prevent system based reordering of sale order lines.
- **website_sale_payment_method_automatic_workflow** Integrates automatic payment creation and invoice reconciliation to eCommerce.